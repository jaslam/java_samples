package me.jangulaslam.javasamples;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

public class Graph {

	static public void runTests() throws Exception {
		{
			/*
			 * 	A 	-> B, D, G
			 * 	B	-> A, E, F
			 * 	C	-> F, H
			 * 	D	-> A, F, G
			 *  E	-> B, G
			 *  F	-> B, C
			 *  G	-> A, E
			 *  H	-> C
			 */
			
			Graph g = new Graph(false);
			// add vertices
			g.addVertex("A");
			g.addVertex("B");
			g.addVertex("C");
			g.addVertex("D");
			g.addVertex("E");
			g.addVertex("F");
			g.addVertex("G");
			g.addVertex("H");
			
			// add edges
			g.addEdge("A", "B"); g.addEdge("A", "D"); g.addEdge("A", "G");
			g.addEdge("B", "A"); g.addEdge("B", "E"); g.addEdge("B", "F");
			g.addEdge("C", "F"); g.addEdge("C", "H");
			g.addEdge("D", "A"); g.addEdge("D", "F");
			g.addEdge("E", "B"); g.addEdge("E", "G");
			g.addEdge("F", "B"); g.addEdge("F", "C");
			g.addEdge("G", "A"); g.addEdge("G", "E");
			g.addEdge("H", "C");
			
			// print
			g.print();
			g.depthFirstTraversal("A");
			g.breadthFirstTraversal("A");
			g.suggestFriends("A");
			g.suggestFriends("B");
		}

		{
		}
	}

	public class Edge {
		public String from;
		public String to;
		public int weight;
		
		public Edge(String from, String to, int weight) {
			this.from = from;
			this.to = to;
			this.weight = weight;
		}
	}
	
	// Set of vertices
	public boolean isWeighted;
	public Map<String, List<Edge>> vertices;
	
	public Graph(boolean isWeighted) {
		this.isWeighted = isWeighted;

		vertices = new HashMap<String, List<Edge>>();
	}

	void addVertex(String data) throws Exception {
		if (vertices.containsKey(data)) {
			throw new Exception("Can't repeat data for the graph");
		}
		
		vertices.put(data, new ArrayList<Edge>());
	}
	
	void addEdge(String from, String to, int weight) throws Exception {
		if (!isWeighted && weight != 1) {
			throw new Exception("Weight has to be 1 because it is a unweighted graph.");
		}
		
		if (!vertices.containsKey(from)) {
			throw new Exception("Vertex from not found in graph.");
		}
		
		if (!vertices.containsKey(to)) {
			throw new Exception("Vertex to not found in graph.");
		}
		
		// add the edge
		List<Edge> edges = vertices.get(from);
		for (Edge edge: edges) {
			if (edge.to == to) {
				throw new Exception("Edge from & to already added");
			}
		}
		
		edges.add(new Edge(from, to, weight));
	}
	
	void addEdge(String from, String to) throws Exception {
		addEdge(from,  to, 1);
	}
	
	void print() {
		System.out.println("Graph");
		System.out.println("Vertices\t\tEdges");
		System.out.println("--------------------------------------------------");
		
		Set<String> vertices = this.vertices.keySet();
		for (String vertex: vertices) {
			System.out.print("   " + vertex + "\t\t");
			List<Edge> edges = this.vertices.get(vertex);
			for (Edge edge: edges) {
				if (isWeighted) {
					System.out.print(" (" + edge.to + ", " + edge.weight + ")");
				} else {
					System.out.print("  " + edge.to);
				}
			}
			System.out.println();
 		}
	}
	
	void depthFirstTraversal(String start) throws Exception {
		if (!vertices.containsKey(start)) {
			throw new Exception("Vertex start not found in graph.");
		}
		
		Set<String> visited = new HashSet<>();
		Stack<String> st = new Stack<>();
		
		System.out.print("\nDepth First Traversal (DFS): ");
		String current = start;
		// push the first (start) into a stack
		st.push(current);
		while (!st.isEmpty()) {
			// look at the top of stack for current vertex
			current = st.peek();
			// if not visited yet, print it and mark it visited
			if (! visited.contains(current)) {
				System.out.print("   " + current);
				visited.add(current);
			}
			
			// boolean to hold nothing left to visit more
			boolean allVisited = true;
			for (Edge edge: vertices.get(current)) {
				// pick one unvisited vertex and push to stack for visiting 
				if (!visited.contains(edge.to)) {
					st.push(edge.to);
					allVisited = false;
					break;
				}
			}
			// if all visited, then pop stack to move back
			if (allVisited) {
				current = st.pop();
			}
		}
	}
	
	void breadthFirstTraversal(String start) throws Exception {
		if (!vertices.containsKey(start)) {
			throw new Exception("Vertex start not found in graph.");
		}
	
		Set<String> visited = new HashSet<>();
		Queue<String> queue = new LinkedList<>();

		System.out.print("\nBreadth First Traversal (BFS): ");
		String current = start;
		do {
			// if not visited yet, print it and mark it visited
			if (! visited.contains(current)) {
				System.out.print("   " + current);
				visited.add(current);
			}
			
			for (Edge edge: vertices.get(current)) {
				// pick one unvisited vertex and add to queue for visiting later
				if (!visited.contains(edge.to)) {
					// add to queue to come back later
					queue.add(edge.to);
					
					// if not visited yet, print it and mark it visited
					if (! visited.contains(current)) {
						System.out.print("   " + current);
						visited.add(current);
					}
				}
			}
			
			// when all edges of the current vertex have been visited, remove another vertex from queue
			current = queue.remove();
		} while (! queue.isEmpty());
	}
	
	private Map<String, Integer> sortMapUsingValue(Map<String, Integer> unsortedMap) {
		// first create a list of map entry object
		List<Entry<String, Integer>> list = new LinkedList<>(unsortedMap.entrySet());
		
		// then sort above list
		Collections.sort(list, new Comparator<Entry<String, Integer>>() {
			@Override
			public int compare(Entry<String, Integer> arg0,
					Entry<String, Integer> arg1) {
				return (arg1.getValue()).compareTo(arg0.getValue());
			}
		});
		
		// re-create the map based on above sorted list
		Map<String, Integer> sortedMap = new LinkedHashMap<>();
		for (Iterator<Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
			Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}
	
	public Set<String> suggestFriends(String person) throws Exception {
		if (!vertices.containsKey(person)) {
			throw new Exception("Vertex person not found in graph.");
		}

		System.out.println("\nSuggesting friends: ");
		Map<String, Integer> friendsOfFriends = new HashMap<>();
		for (Edge myRelationship: vertices.get(person)) {
			System.out.print("My friend: " + myRelationship.to + ", thier friends: ");
			for (Edge theirRelationship: vertices.get(myRelationship.to)) {
				if (theirRelationship.to != person) {
					System.out.print(" " + theirRelationship.to);
					// if a friend of friend is common, inc
					if (!friendsOfFriends.containsKey(theirRelationship.to)) {
						friendsOfFriends.put(theirRelationship.to, 1);
					} else {
						friendsOfFriends.put(theirRelationship.to, friendsOfFriends.get(theirRelationship.to) + 1);
					}
				}
			}
			System.out.println();
		}
		
		//friendsOfFriends.put("C", 5);
		System.out.println("Suggested friends (map): " + friendsOfFriends);
		
		// sort the map- (keys) based on key's values
		friendsOfFriends = sortMapUsingValue(friendsOfFriends);
		
		Set<String> results = friendsOfFriends.keySet();
		System.out.println("Suggested friends (set): " + results);
		
		return results;
	}

}
