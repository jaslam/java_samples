package me.jangulaslam.javasamples;

import java.util.Date;
import java.util.Stack;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class DynamicProgramming {

	private static final Logger log4j = LogManager.getLogger(Fibonacci.class.getName());
	
	static public void runTests() throws Exception {
		/*
		int n = 6;
		{
			//log4j.trace("before");
			Date before = new Date(); 
			System.out.println("Ways for climbing " + n + " stairs using 1 or 2 steps (recursive method) is " + countWaysOneOrTwoSteps(n));
			Date after = new Date(); 
			//log4j.trace("after");
			log4j.info("Time taken for recursive algorithm " + (after.getTime() - before.getTime()) + " ms");
		}

		{
			//log4j.trace("before");
			Date before = new Date(); 
			System.out.println("Ways for climbing " + n + " staris using 1 or 2 steps (dynamic-programming method) is " + countWaysOneOrTwoStepsDP(n));
			Date after = new Date(); 
			//log4j.trace("after");
			log4j.info("Time taken for dynammic-programming algorithm " + (after.getTime() - before.getTime()) + " ms");
		}

		{
			//log4j.trace("before");
			Date before = new Date(); 
			System.out.println("Ways for climbing " + n + " staris using 1 or 2 or 3 steps (recursive method) is " + countWaysOneOrTwoOrThreeSteps(n));
			Date after = new Date(); 
			//log4j.trace("after");
			log4j.info("Time taken for recursive algorithm " + (after.getTime() - before.getTime()) + " ms");
		}

		{
			//log4j.trace("before");
			Date before = new Date(); 
			System.out.println("Ways for climbing " + n + " staris using 1 or 2 or 3 steps (dynamic-programming method) is " + countWaysOneOrTwoOrThreeStepsDP(n));
			Date after = new Date(); 
			//log4j.trace("after");
			log4j.info("Time taken for dynammic-programming algorithm " + (after.getTime() - before.getTime()) + " ms");
		}
		*/	
		
		/*
		{
			//int[] array = {7, 4, 10, 12, 3, 5, 7, 2, 14, 1};
			int[] array = {3, 5, 7, 2, 10, 20, 4, -2, 0, 10};
			Arrays.print(array);
			getLongestIncreasingSubsequence(array);
		}		
		{
			int[] array = {7, 4, 10, 12, 3, 5, 7, 2, 14, 1};
			Arrays.print(array);
			getLongestIncreasingSubsequence(array);
		}	
		{
			int[] array = Arrays.getRandomArray(20);
			Arrays.print(array);
			getLongestIncreasingSubsequence(array);
		}	

		{
			int[] array = {10, 20, 70, -10, 50, 60, 90, -20, 30, 100};
			Arrays.print(array);
			getLongestIncreasingSubsequence(array);
		}	

		{
			int[] array = {10, 20, 70, -10, 50, 60, 90, -20, 30, 100};
			Arrays.print(array);
			getMaxSumIncreasingSubsequence(array);
		}	
		*/	
		
		{
			getLongestCommonSequenceDP("abcdef", "zbcdf");
			getLongestCommonSequenceDP("abcdefdghji", "zbcdfbcdefasd");
		}
	}

	// recursive method
	public static long countWaysOneOrTwoSteps(int n) {
		if (n == 0) return 1;
		if (n == 1) return 1;
		return countWaysOneOrTwoSteps(n - 1) + countWaysOneOrTwoSteps(n - 2);
	}

	// dynamic programming  method (memorization and recursive)
	private static long results[] = new long[4096];
	public static long countWaysOneOrTwoStepsDP(int n) {
		if (n == 0) return 1;
		if (n == 1) return 1;
		if (results[n] == 0) {
			results[n] = countWaysOneOrTwoStepsDP(n - 1) + countWaysOneOrTwoStepsDP(n - 2);
		}
		return results[n];
	}
	
	// recursive method
	public static long countWaysOneOrTwoOrThreeSteps(int n) {
		if (n < 0) return 0;
		if (n == 0) return 1;
		return countWaysOneOrTwoOrThreeSteps(n - 1) + countWaysOneOrTwoOrThreeSteps(n - 2) + countWaysOneOrTwoOrThreeSteps(n - 3);
	}

	// dynamic programming  method (memorization and recursive)
	private static long results2[] = new long[4096];
	public static long countWaysOneOrTwoOrThreeStepsDP(int n) {
		if (n < 0) return 0;
		if (n == 0) return 1;
		if (results2[n] == 0) {
			results2[n] = countWaysOneOrTwoOrThreeStepsDP(n - 1) + countWaysOneOrTwoOrThreeStepsDP(n - 2) + countWaysOneOrTwoOrThreeStepsDP(n - 3);
		}
		return results2[n];
	}
	

	// using dynamic programming
	public static int getLongestIncreasingSubsequence(int[] array) {
		int result = 1;
		
		int[] maxLength = new int[array.length];
		
		for (int i = 0; i < maxLength.length; ++i) {
			maxLength[i] = 1;	// initialize to 1, saying the min subsequence length at any index is just that element (1)
			
			for (int j = 0; j < i; ++j) {
				if (array[j] < array[i] && maxLength[j] >= maxLength[i]) {
					maxLength[i] = 1 + Math.max(maxLength[i], maxLength[j]);
				}
			}
		}
		
		// find max in maxLength array
		try {
			result = Arrays.findMax(maxLength);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//Arrays.print(maxLength);
		System.out.println("Length of increasing subsequences: " + result);
		return result;
	}	
	
	// using dynamic programming
	public static int getMaxSumIncreasingSubsequence(int[] array) {
		int result = 1;
		
		int[] maxSum = new int[array.length];
		int[] indices = new int[array.length];
		
		for (int i = 0; i < maxSum.length; ++i) {
			maxSum[i] = array[i];	// initialize to same number, saying the max sum will at least contain the # at the index
			indices[i] = i;			// above maxSum is from the same index
			
			for (int j = 0; j < i; ++j) {
				if (array[j] < array[i]) {
					if (maxSum[j] + array[i] > maxSum[i]) {
						// save index
						indices[i] = j;
						// save max sum
						maxSum[i] = maxSum[j] + array[i];
					}
				}
			}
		}
		
		// find max in maxSum array
		try {
			result = Arrays.findMax(maxSum);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Arrays.print(maxSum);
		System.out.println("Max Sum of increasing subsequence elements: " + result);

		Arrays.print(indices);
		// print the subsequence using indices array
		Stack<Integer> st = new Stack<>();
		try {
			int i = Arrays.findMaxIndex(maxSum);
			st.push(array[i]);
			while (i > 0) {
				i = indices[i];
				st.push(array[i]);
			}
			
			System.out.println("Max sum subsequence: ");
			while(!st.isEmpty()) {
				System.out.format("[%4d] ", st.pop());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		

		return result;
	}	
	
	// Using dynamic programming
	public static int getLongestCommonSequenceDP(String s1, String s2) {
		int result = 0;
		
		System.out.println("s1: " + s1 + ", s2: " + s2);
		int[][] temp = new int[s2.length() + 1][s1.length() + 1];
		System.out.println("Before: ");
		Matrices.printMatrix(temp);
		
		for (int i = 0; i < s2.length(); ++i) {
			for (int j = 0; j < s1.length() ; ++j) {
				if (s1.charAt(j) == s2.charAt(i)) {
					temp[i+1][j+1] = 1 + temp[i][j];
					result = Math.max(result, temp[i+1][j+1]);
				} else {
					temp[i+1][j+1] = 0;
				}
			}
		}
		
		System.out.println("After: ");
		Matrices.printMatrix(temp);
		
		System.out.println("Longest common subsequence: " + result);
		return result;
	}
}

