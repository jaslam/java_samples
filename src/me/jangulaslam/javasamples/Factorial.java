package me.jangulaslam.javasamples;

import java.util.Date;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Factorial {

	private static final Logger log4j = LogManager.getLogger(Fibonacci.class.getName());
	
	static public void runTests() throws Exception {
		int n = 30;
		
		// evaluate PRN expression
		{
			//log4j.trace("before");
			Date before = new Date(); 
			System.out.println("Factorial of " + n + " using recursive method is " + getR(n));
			Date after = new Date(); 
			//log4j.trace("after");
			log4j.info("Time taken for recursive algorithm " + (after.getTime() - before.getTime()) + " ms");
		}

		{
			Date before = new Date(); 
			System.out.println("Factorial of " + n + " using dynamic-programming method is " + getDP(n));
			Date after = new Date(); 
			log4j.info("Time taken for dynamic-programming algorithm " + (after.getTime() - before.getTime()) + " ms");
		}
	}

	// recursive method
	public static double getR(int n) {
		if (n == 0) return 1;
		if (n == 1) return 1;
		return n * getR(n - 1);
	}

	// dynamic programming  method (memorization and recursive)
	private static double results[] = new double[4096];
	public static double getDP(int n) {
		if (n == 0) return 1;
		if (n == 1) return 1;
		if (results[n] == 0) {
			results[n] = n * getDP(n - 1);
		}
		return results[n];
	}

}
