package me.jangulaslam.javasamples;

public class BTNode {
	public int data;
	public BTNode left;
	public BTNode right;
	
	BTNode(int data) {
		this.data = data;
	}
}
