package me.jangulaslam.javasamples;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Arrays {
	public static void runTests() {
		int[] A = {3, 2, 4,};
		print(A);
		int[] results = twoSum(A, 6);
		print(results);
	}


	public static int[] twoSum(int[] nums, int target) {
	    Map<Integer, Integer> map = new HashMap<>();
	    for (int i = 0; i < nums.length; i++) {
	        int complement = target - nums[i];
	        if (map.containsKey(complement)) {
	            return new int[] { map.get(complement), i };
	        }
	        map.put(nums[i], i);
	    }
	    throw new IllegalArgumentException("No two sum solution");
	}	
	
	static public int findMax(int[] array) throws Exception {
		if (array == null) {
			throw new NullPointerException();
		}
		
		if (array.length == 0) {
			throw new Exception("Empty array");
		}
		
		int max = array[0];
		for (int i = 1; i < array.length; ++i) {
			if (array[i] > max) {
				max = array[i];
			}
		}
		
		return max;
	}

	static public int findMaxIndex(int[] array) throws Exception {
		if (array == null) {
			throw new NullPointerException();
		}
		
		if (array.length == 0) {
			throw new Exception("Empty array");
		}
		
		int max = array[0], i = 0;
		for (i = 1; i < array.length; ++i) {
			if (array[i] > max) {
				max = array[i];
			}
		}
		
		return i-1;
	}
	
	static private Random rand = new Random();

	static int[] getSequenceArray(int count) {
		int array[] = new int[count];
		
		for (int i = 0; i < array.length; ++i) {
			array[i] = i;
		}
		
		return array;
	}

	static int[] getRandomArray(int count) {
		int array[] = new int[count];
		
		for (int i = 0; i < array.length; ++i) {
			array[i] = rand.nextInt(count);
		}
		
		return array;
	}

	static int[] getRandomUniqueArray(int count) {
		ArrayList<Integer> fromArray = new ArrayList<>();
		for (int i = 0; i < count; ++i) {
			fromArray.add(i);
		}
		
		int array[] = new int[count];
		int randomIndex = 0;
		for (int i = 0; i < count; ++i) {
			randomIndex = rand.nextInt(fromArray.size());
			array[i] = fromArray.get(randomIndex);
			fromArray.remove(randomIndex);
		}
		
		return array;
	}
	
	static void swap(int[] array, int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
	
	static void print(int[] array) {
		//System.out.println();
		System.out.println("\nArray of length " + array.length + ": ");
		for (int i = 0; i < array.length; ++i) {
			System.out.format(" %4d  ", i);
		}
		System.out.println();
		for (int i = 0; i < array.length; ++i) {
			System.out.format("[%4d] ", array[i]);
		}
		System.out.println();
	}
}
