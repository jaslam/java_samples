package me.jangulaslam.javasamples;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;
import java.util.StringTokenizer;
import static java.lang.Math.toIntExact;



public class Others {
	public static void runTests() {
		//towerOfHanoi();
		//towerOfHanoi2();
		//fizzBuzzWoof(24);
		//System.out.print("Result: " + wordMachine("13 DUP 4 POP 5 DUP + DUP + -"));
		//System.out.print("Result: " + wordMachine("5 6 + -"));
		//System.out.print("Result: " + wordMachine("3 DUP 5 - -"));
		virtualKeyboard("ABCFAMILY");
	}
	
    public static int wordMachine(String S) {
        // write your code in Java SE 8

    	//System.out.println(S);
    	Stack<Long> stack = new Stack<>();
    	
    	StringTokenizer tokens = new StringTokenizer(S);
    	
    	String token = "";
    	while (tokens.hasMoreTokens()) {
    		token = tokens.nextToken();
    		//System.out.println(token);
    		
    		try {
        		switch (token) {
        		case "POP":
        			stack.pop();
        			break;
        			
        		case "DUP":
        			stack.push(stack.peek());
        			break;
        			
        		case "+":
        			stack.push(stack.pop() + stack.pop());
        			break;
        			
        		case "-":
        			long value = stack.pop() - stack.pop();
        			if (value < 0) {
        				//System.out.println("-ve value can't be pushed");
        				return -1;
        			}
        			stack.push(value);
        			break;
        			
        		default:
        			try {
            			int number = Integer.parseInt(token);
            			stack.push((long)number);
        			} catch (NumberFormatException e) {
        				//System.out.println("Exception: " + e);
        				return -1;
        			}
        		}
    		} catch (EmptyStackException e) {
				//System.out.println("Exception: " + e);
    			return -1;
    		}
    	}
    	
    	return !stack.isEmpty() ? toIntExact(stack.peek()) : -1;
    }
	
    public static void fizzBuzzWoof(int N) {
        // write your code in Java SE 8
        
    	// create a map for corresponding values of strings for 3, 5, 7 as keys 
        Map<Integer, String> map = new HashMap<>();
        map.put(3, "Fizz");
        map.put(5, "Buzz");
        map.put(7, "Woof");
        
        // loop from 1 to N and see if the number is divisible by 3, 5, 7 using modulus 
        // operator and print values accordingly using the above map or the number iteself
        boolean bPrinted;
        for (int i = 1; i <=  N; ++i) {
        	bPrinted = false;
        	if (i % 3 == 0) {
        		System.out.print(map.get(3));
        		bPrinted = true;
        	} 
        	
        	if (i % 5 == 0) {
        		System.out.print(map.get(5));
        		bPrinted = true;
        	} 
        	
        	if (i % 7 == 0) {
        		System.out.print(map.get(7));
        		bPrinted = true;
        	}
        	
        	if (!bPrinted) {
        		System.out.print(i);
        	}
        	
        	System.out.println();
        }
    }

    public static void virtualKeyboard(String str) {
    	class Position {
    		public int x;
    		public int y;
    		public Position(int x, int y) {
    			this.x = x;
    			this.y = y;
    		}
    	}
    	Map<Character, Position> keyboard = new HashMap<>();
    	
    	String all = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    	char[] chars = all.toCharArray();
    	int x = 0, y = -1;
    	for (int i = 0; i < chars.length; ++i) {
    		x = i % 6;
    		if (x == 0) {
    			++y;
    		}
    		
    		keyboard.put(chars[i], new Position(x, y));
    	}
    	
//    	Set<Character> keys = keyboard.keySet();
//    	for(Character key : keys) {
//    		Position pos = keyboard.get(key);
//    		System.out.println("key: " + key.charValue() + ", (pos-x: " + pos.x + ", pos-y: " + pos.y + ")");
//    	}
    	
    	for (int i = 0; i < str.length(); ++i) {
    		Position pos = keyboard.get(str.charAt(i));
    		System.out.println("key: " + str.charAt(i) + ", (pos-x: " + pos.x + ", pos-y: " + pos.y + ")");
    	}
    }
    
	// tower of hanoi (using int to indicate poles)
	private static void towerOfHanoi(int n, int src, int spare, int dest) {
		if (n > 0) {
			towerOfHanoi(n - 1, src, dest, spare);
			System.out.format("Moving disk: %d from source pole: %d to destination pole: %d (spare pole: %d)\n", n, src, dest, spare);
			towerOfHanoi(n - 1, spare, src, dest);
		}
	}
	
	public static void towerOfHanoi() {
		towerOfHanoi(1, 1, 2, 3);
	}

	// tower of hanoi2 (using Stacks to indicate poles)
	private static void towerOfHanoi2(int n, Stack<Integer> src, Stack<Integer> dest, Stack<Integer> spare) {
		//System.out.format("\nRunning => Source pole: %s, Destination pole: %s, Spare pole: %s, n: %d\n", src, dest, spare, n);
		if (n > 0) {
			towerOfHanoi2(n - 1, src, spare, dest);
			//System.out.format("Before moving disk: %d from source pole: %s to destination pole: %s (spare pole: %s)\n", n, src, dest, spare);
			dest.push(src.pop());
			System.out.format("After moving disk: %d from source pole: %s to destination pole: %s (spare pole: %s)\n", n, src, dest, spare);
			towerOfHanoi2(n - 1, spare, dest, src);
		}
	}

	public static void towerOfHanoi2() {
		Stack<Integer> src = new Stack<>();
		Stack<Integer> dest = new Stack<>();
		Stack<Integer> spare = new Stack<>();

		for (int i = 3; i > 0; --i) {
			src.push(i);
		}
		
		System.out.format("Before => Source pole: %s, Destination pole: %s, Spare pole: %s\n", src, dest, spare);
		towerOfHanoi2(src.size(), src, dest, spare);
		System.out.format("After => Source pole: %s, Destination pole: %s, Spare pole: %s\n", src, dest, spare);
	}
}
