/**
 * 
 */
package me.jangulaslam.javasamples;



/**
 * @author Jangul
 *
 */
public class Runner {
    static int maxLength(int[] a, int k) {
        int length = 0;
        
        for (int i = 0; i < a.length; ++i) {
            int sum = 0;
            for (int j = i; j < a.length; ++j) {
                sum += a[j];
	                if (sum <= k) {
	                    length = Math.max(length, j - i + 1);
	                } else {
	                	break;
	                }
            }
        }

        return length;
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			//StacksAndQueues.runTests();
			//int[] array1 = Utils.getRandomArray(50);
			//Utils.printArray(array1);
			
			//SortingAlgorithms.runTests();
			
			int[] a = {3, 1, 2, 1};
			maxLength(a, 4);
//			int[] a = {1, 2, 3};
//			maxLength(a, 4);
			
			
			//Fibonacci.runTests();
			//Factorial.runTests();
			
			//LinkedList.runTests();
			
			//DynamicProgramming.runTests();
			
			//Matrices.runTests();
			
			//Trees.runTests();
			//Graph.runTests();
			
			//PriorityQueues.runTests();
			
			//Concurrency.runTests();
			
			//Strings.runTests();
			//Arrays.runTests();
			
			//Others.runTests();
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
