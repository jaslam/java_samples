package me.jangulaslam.javasamples;

import java.util.HashMap;
import java.util.Map;

public class Strings {
	public static void runTests() {
//		System.out.println(Strings.reverse("hello"));
//		System.out.println(Strings.reverse(""));
//		System.out.println(Strings.reverse("h"));
		
//		System.out.println(Strings.reverseWords("How are you doing"));
//		System.out.println(Strings.reverseWords("I'm doing good"));
//		System.out.println(Strings.reverseWords(""));
//		System.out.println(Strings.reverseWords("a"));
		
//		Strings.printPermutations("a");
//		Strings.printPermutations("ab");
		Strings.printPermutations("abc");
//		Strings.printPermutations("abcd");
		
		//Strings.printInterleaves("ab", "cd");
		//Strings.printInterleaves("abc", "def");
		
		//Strings.printCombinations("abc");
		//Strings.printCombinationsOfSize("abcdef", 4);
		
		//Strings.printPossibleAlphabetsOfPhoneNumber("234");
	}

	private static void swap(char[] str, int i, int j) {
		char ch = str[i];
		str[i] = str[j];
		str[j] = ch;
	}

	private static void reverse(char[] s, int start, int end) {
		for (int i = start, j = end; i < j; ++i, --j) {
			swap(s, i, j);
		}
	}
	
	public static String reverse(String str) {
		char[] s = str.toCharArray();
		reverse(s, 0, s.length - 1);
		return new String(s);
	}

	public static String reverseWords(String str) {
		char[] s = str.toCharArray();
		// first reverse the whole string
		reverse(s, 0, s.length - 1);
		
		// now reverse each words
		int i = 0;
		for (int j = 0; j <= s.length; ++j) {
			if (j == s.length || s[j] == ' ') {
				reverse(s, i, j - 1);
				i = j + 1;
			}
		}
		
		return new String(s);
	}
	
	// string permutations
	private static void printPermutations(char[] str, int start, int end) {
		System.out.println("printPermutations (str: " + new String(str) + " start: " + start + ", end: " + end + ")");
		if (start > end) {
			System.out.println(str);
			return;
		}
			
		for (int i = start; i <= end; ++i) {
			swap(str, start, i);
			printPermutations(str.clone(), start + 1, end);
		}
	}
	
	
	private static void permutations(String str, int start, StringBuffer sb) {
		if  (sb.length() == str.length() ) {
			System.out.println(sb);
			sb.deleteCharAt(sb.length() - 1);
			return;
		}
		
		for (int i = start; i < str.length(); ++i) {
			sb.append(str.charAt(i));
			permutations(str, start + 1, sb);
		}
	}
	
	public static void printPermutations(String input) {
		System.out.print("\nPermutations of \"" + input + "\": \n");
		//char[] str = input.toCharArray();
		//printPermutations(str, 0, str.length - 1);
		permutations(input, 0, new StringBuffer());
	}
	
	
	// string interleaving
	private static void printInterleaves(char[] str1, int pos1, char[] str2, int pos2, char[] str, int pos) {
		//System.out.println("\t entering s1: [" + pos1 + "], s2: [" + pos2 + "]");
		
		if (pos1 == str1.length && pos2 == str2.length) {
			System.out.println(str);
		}
		if (pos1 != str1.length) {
			str[pos] = str1[pos1];
			printInterleaves(str1, pos1 + 1, str2, pos2, str, pos + 1);
		}
		if (pos2 != str2.length) {
			str[pos] = str2[pos2];
			printInterleaves(str1, pos1, str2, pos2 + 1, str, pos + 1);
		}

		//System.out.println("\t returning s1: [" + pos1 + "], s2: [" + pos2 + "],  s3: [" + pos + "]");
	}
	
	public static void printInterleaves(String str1, String str2) {
		System.out.print("\nInterleaving \"" + str1 + "\" & \"" + str2 + "\":\n");

		char[] str = new char[str1.length() + str2.length() + 1];
		printInterleaves(str1.toCharArray(), 0, str2.toCharArray(), 0, str, 0);
	}
	

	// all string combinations
	public static void printCombinations(String str, int pos, StringBuffer sb) {
		if (pos == str.length()) {
			return;
		}
		
		for (int i = pos; i < str.length(); ++i) {
			sb.append(str.charAt(i));
			System.out.println(sb);
			printCombinations(str, i + 1, sb);
			sb.deleteCharAt(sb.length() - 1);
		}
	}

	public static void printCombinations(String str) {
		System.out.print("\nCombinations of \"" + str + "\":\n");
		StringBuffer sb = new StringBuffer(str.length());
		printCombinations(str, 0, sb);
	}

	// all string combinations
	public static void printCombinationsOfSize(String str, int pos, StringBuffer sb, int size) {
		if (pos == str.length()) {
			return;
		}
		
		for (int i = pos; i < str.length(); ++i) {
			sb.append(str.charAt(i));
			if (sb.length() == size) {
				System.out.println(sb);
			}
			printCombinationsOfSize(str, i + 1, sb, size);
			sb.deleteCharAt(sb.length() - 1);
		}
	}

	public static void printCombinationsOfSize(String str, int size) {
		System.out.print("\nCombinations of \"" + str + "\" of size " + size + ":\n");
		StringBuffer sb = new StringBuffer(str.length());
		printCombinationsOfSize(str, 0, sb, size);
	}
	
	public static void printPossibleAlphabetsOfPhoneNumber(String phoneNumber) {
		Map<Character, String> map = new HashMap<>();
		
		map.put('0',  "");
		map.put('1',  "");
		map.put('2',  "ABC");
		map.put('3',  "DEF");
		map.put('4',  "GHI");
		map.put('5',  "JKL");
		map.put('6',  "MNO");
		map.put('7',  "PQRS");
		map.put('8',  "TUV");
		map.put('9',  "WXYZ");
		
		StringBuilder sb = new StringBuilder();
		printAlphabets(phoneNumber, 0, sb, map);
	}
	private static void printAlphabets(String phoneNumber, int pos, StringBuilder sb, Map<Character, String> map) {
		if (sb.length() == phoneNumber.length()) {
			System.out.println(sb.toString());
			return;
		}
		
		char ch = phoneNumber.charAt(pos);
		String values = map.get(ch);
		if (values.length() == 0) {
			sb.append(ch);
			printAlphabets(phoneNumber, pos + 1, sb, map);
			sb.deleteCharAt(sb.length() - 1);
		} else {
			for (int i = 0; i < values.length(); ++i) {
				sb.append(values.charAt(i));
				printAlphabets(phoneNumber, pos + 1, sb, map);
				sb.deleteCharAt(sb.length() - 1);
			}
		}
	}
}
