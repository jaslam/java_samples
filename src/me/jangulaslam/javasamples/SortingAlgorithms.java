/**
 * 
 */
package me.jangulaslam.javasamples;

/**
 * @author Jangul
 *
 */
public class SortingAlgorithms {
	public static void runTests() throws Exception {
		{
			int array[] = {7, 4, 10, 12, 3, 5, 7, 2, 14, 1};
			Arrays.print(array);
			sortbyModulo(array, 3);
			Arrays.print(array);
		}

		{
			int array[] = {1,2,3,4,5};
			Arrays.print(array);
			sortbyModulo(array, 3);
			Arrays.print(array);
		}
	}
	
	public static void sortbyModulo(int[] array, int which) {
		int partition = 0;
		for (int search = 0; search < which; ++search) {
			for (int i = partition; i < array.length; ++i) {
				if (array[i] % which == search) {
					if (i != partition) {
						Arrays.swap(array, i, partition);
					}
					++partition;
				}
			}
		}
	}
}
