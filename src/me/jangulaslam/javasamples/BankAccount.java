package me.jangulaslam.javasamples;

import java.util.HashSet;
import java.util.Set;

public class BankAccount {
	public Set<String> names = new HashSet<>();
	private double balance;
	
	public BankAccount(String name, double openingAmount) {
		this.names.add(name);
		this.balance = openingAmount;
		Logger.log("opening balance: " + balance);
	}
	
	public Set<String> addJointName(String name) {
		this.names.add(name);
		return names;
	}
	
	public double deposit(double amount) throws Exception {
	//public synchronized double deposit(double amount) throws Exception {
		//Logger.log("depositing: " + amount);
		if (amount <= 0) {
			throw new Exception("Deposit amount can't be zero or negative");
		}
		
		synchronized(this) {
			balance += amount;
		}
		Logger.log("balance after deposit: " + balance);
		return balance;
	}
	
	public double withdraw(double amount) throws Exception {
	//public synchronized double withdraw(double amount) throws Exception {
		//Logger.log("withdrawing: " + amount);
		if (balance <= 0) {
			throw new Exception("Can't withdraw from zero or negative balance");
		}
		
		synchronized(this) {
			balance -= amount;
		}
		Logger.log("balance after withdraw: " + balance);
		return balance;
	}

	public double balance() {
		//Logger.log("balance: " + balance);
		return balance;
	}
}
