package me.jangulaslam.javasamples;

public class Job implements Runnable {
	public int total;
	
	public Job(int total) {
		this.total = total;
	}
	
	@Override
	public void run() {
		Logger.log("Worker thread has started");
		for (int i = 0; i < 10; ++i) {
			Logger.log("  " + i);
			try {
				Thread.sleep((this.total/2) * 100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Logger.log("Worker thread is exiting...");
	}
}
