package me.jangulaslam.javasamples;


public class Matrices {

	public static void runTests() throws Exception {
//		// print matrix in spiral fashion
//		{
//			{
//				int m[][] = {{1,2,3,4,5}, {6,7,8,9,10}, {11,12,13,14,15}, {16,17,18,19,20}, {21,22,23,24,25}};
//				printMatrix(m);
//				printMatrixSpiral(m);
//				System.out.println();
//			}
//
//			{
//				int m[][] = {{1,2,3,4,5}, {6,7,8,9,10}, {11,12,13,14,15}};
//				printMatrix(m);
//				printMatrixSpiral(m);
//				System.out.println();
//			}
//
//			{
//				int m[][] = {{1,2,3,4,5}, {6,7,8,9,10}};
//				printMatrix(m);
//				printMatrixSpiral(m);
//				System.out.println();
//			}
//
//			{
//				int m[][] = {{1,2,3,4,5}};
//				printMatrix(m);
//				printMatrixSpiral(m);
//				System.out.println();
//			}
//			
//			{
//				int m[][] = {{1}, {2}, {3}, {4}, {5}};
//				printMatrix(m);
//				printMatrixSpiral(m);
//				System.out.println();
//			}
//
//			{
//				int m[][] = {{1, 2}, {3, 4}, {5, 6}, {7, 8}, {9, 10}};
//				printMatrix(m);
//				printMatrixSpiral(m);
//				System.out.println();
//			}
//		}
//
//		// rotate matrix
//		{
//			{
//				int m[][] = {{1,2,3,4,5}, {6,7,8,9,10}, {11,12,13,14,15}, {16,17,18,19,20}, {21,22,23,24,25}};
//				printMatrix(m);
//				System.out.println();	
//				rotateMatrix(m);
//				printMatrix(m);
//				System.out.println();	
//			}
//
//			{
//				int m[][] = {{1,2,3,4}, {5,6,7,8}, {9,10,11,12}, {11,12,13,14}};
//				printMatrix(m);
//				System.out.println();	
//				rotateMatrix(m);
//				printMatrix(m);
//				System.out.println();	
//			}
//
//			{
//				int m[][] = {{1,2,3}, {4,5,6}, {7,8,9}};
//				printMatrix(m);
//				System.out.println();	
//				rotateMatrix(m);
//				printMatrix(m);
//				System.out.println();	
//			}
//			
//			{
//				int m[][] = {{1,2}, {3,4}};
//				printMatrix(m);
//				System.out.println();	
//				rotateMatrix(m);
//				printMatrix(m);
//				System.out.println();	
//			}
//		}
		
		// hour glass sum
		{
			int m[][] = {
							{1, 1, 1, 0, 0, 0}, 
							{0, 1, 0, 0, 0, 0}, 
							{1, 1, 1, 0, 0, 0}, 
							{0, 9, 2, -4, -4, 0}, 
							{0, 0, 0, -2, 0, 0}, 
							{0, 0, -1, -2, -4, 0}
						};
			
			printMatrix(m);
			maxSumHourGlass(m);
		}
		
		// mirror matrix (horizontally)
		
		// mirror matrix (vertically)
		
		// mirror matrix (diagonally)
		
		// mirror matrix (reverse-diagonally)
	}

	public static void printMatrix(int[][] m) {
		System.out.println();
		
		System.out.format("  i/j  ");
		for (int j = 0; j < m[0].length; ++j) {
			System.out.format("(% 4d) ", j);
		}
		System.out.println();
		for(int i = 0; i < m.length; ++i) {
			System.out.format("\n(% 4d) ", i);
			for (int j = 0; j < m[i].length; ++j) {
				System.out.format("[% 4d] ", m[i][j]);
			}
			
			System.out.println();
		}
	}

	public static void rotateMatrix(int[][] m) throws Exception {
		if (m.length != m[0].length) {
			// not a square matrix
			throw new Exception("The matrix has to be a square one.");
		}
		
		rotateMatrix(m, 0, 0, m.length - 1, m[0].length - 1);
	}
	
	private static void rotateMatrix(int[][] m, int rowMin, int columnMin, int rowMax, int columnMax) {
		if (rowMin >= rowMax || columnMin >= columnMax) {
			return;
		}
		
		for (int i = 0; i < rowMax - rowMin; ++i) {
			int value = m[rowMin][columnMin + i];
			value = moveElement(m, value, rowMin + i, columnMax);
			value = moveElement(m, value, rowMax, columnMax - i);
			value = moveElement(m, value, rowMax - i, columnMin);
			moveElement(m, value, rowMin, columnMin + i);

			//System.out.format("After rotating all elements in row");
			//printMatrix(m);

		}
		
		//System.out.format("After rotating all elements of all rows");
		//printMatrix(m);
		rotateMatrix(m, ++rowMin, ++columnMin, --rowMax, --columnMax);
	}
	
	private static int moveElement(int[][] m, int movingIn, int rowTo, int columnTo) {
		int movingOut = m[rowTo][columnTo];
		m[rowTo][columnTo] = movingIn;
		return movingOut;
	}
	
	public static void printMatrixSpiral(int[][] m) {
		printMatrixSpiral(m, 0, 0, m.length - 1, m[0].length - 1);
	}
	
	private static void printMatrixSpiral(int[][] m, int rowFrom, int columnFrom, int rowTo, int columnTo) {
		if (rowTo - rowFrom < 0 || columnTo - columnFrom < 0) {
			return;
		}
		
		printRowInRightDirection(m, rowFrom, columnFrom, columnTo - 1);
		printColumnInDownDirection(m, rowFrom, rowTo - 1, columnTo);
		printRowInLeftDirection(m, rowTo, columnTo, columnFrom + 1);
		printColumnInUpDirection(m, rowTo, rowFrom + 1, columnFrom);
		
		printMatrixSpiral(m, ++rowFrom, ++columnFrom, --rowTo, --columnTo);
	}
	
	private static void printRowInRightDirection(int[][] m, int row, int columnFrom, int columnTo) {
		//System.out.print("[");
		for (int j = columnFrom; j <= columnTo; ++j) {
			System.out.format("% 4d ", m[row][j]);
		}
		//System.out.print("]");
	}

	private static void printColumnInDownDirection(int[][] m, int rowFrom, int rowTo, int column) {
		//System.out.print("[");
		for (int i = rowFrom; i <= rowTo; ++i) {
			System.out.format("% 4d ", m[i][column]);
		}
		//System.out.print("]");
	}

	private static void printRowInLeftDirection(int[][] m, int row, int columnFrom, int columnTo) {
		//System.out.print("[");
		for (int j = columnFrom; j >= columnTo; --j) {
			System.out.format("% 4d ", m[row][j]);
		}
		//System.out.print("]");
	}
	
	private static void printColumnInUpDirection(int[][] m, int rowFrom, int rowTo, int column) {
		//System.out.print("[");
		for (int i = rowFrom; i >= rowTo; --i) {
			System.out.format("% 4d ", m[i][column]);
		}
		//System.out.print("]");
	}

    private static int sumHourGlass(int[][] m, int row, int column) {
        return m[row][column] +    m[row][column + 1] +       m[row][column + 2] +
                            m[row + 1][column + 1] +
            m[row + 2][column] +   m[row + 2][column + 1] +   m[row + 2][column + 2];
    }
    
	public static void maxSumHourGlass(int[][] m) {
        int maxSum = Integer.MIN_VALUE;
        for (int i = 0; i < m.length - 2; ++i) {
            for (int j = 0; j < m[i].length - 2; ++j) {
                int sum = sumHourGlass(m, i, j);
                maxSum = Math.max(maxSum, sumHourGlass(m, i, j));
                //System.out.println("i: " + i + ", j: " + j + ", sum: " + sum + ", maxSum: " + maxSum);
            }
        }
        
        System.out.println(maxSum);		
	}
}

